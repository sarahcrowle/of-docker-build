#!/bin/sh

mkdir output
podman build -t picsofbread/openfirm .
podman run -v $(pwd)/output:/output:Z -t picsofbread/openfirm
