# Dockerfile for reproducible x86 OpenFirmware builds

This is a tiny Dockerfile based on Debian stretch i386 for building OpenFirmware 
on modern Linux.

## Why not just build it on my host machine?

It won't work. [See here.](https://wiki.restless.systems/wiki/Building_OpenFirmware_for_QEMU) 

## How do I use it?

Run `build.sh`. (uses podman)

## License

MIT I guess. See LICENSE.txt
