FROM --platform=linux/386 debian:stretch

RUN apt-get update
RUN apt-get install -y build-essential git

CMD cd /output && \
	git clone https://github.com/openbios/openfirmware.git ; \
	cd openfirmware/cpu/x86/pc/emu/build ; \
	make && \
	cp emuofw.rom /output && \
	printf "\033[0;36m  Check the output dir for your ROM :^)  \033[0m\n"
